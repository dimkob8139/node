const GenerateUniqueToken=require('../services/generateUniqToken')
const sequelize = require("../db");
const {PasswordReset} = require("../models/PasswordReset");
const {User} = require("../models/User");
const {PasswordRecoveryEmail} = require("../mails/passwordRecoveryEmail");

class PasswordRecoveryController{
    passwordForgot=async (req,res)=>{
        console.log(req.body.email)
      const user= await User.findOne({
          where: {
              email: req.body.email
          }

        })
        if(user){
            const t=await sequelize.transaction()
            try {
                await PasswordReset.destroy({
                    where: {
                        email: req.body.email
                    }
                }, {transaction:t})
                let exisstingToken=null
                let token=null

                do {

                    let token = GenerateUniqueToken.generateToken()
                    exisstingToken = await PasswordReset.findOne({
                        where: {
                            token: token
                        }
                    })

                }while (exisstingToken!==null)


                await PasswordReset.create({email: user.email, token: token},
                    {transaction:t})
                let email = new PasswordRecoveryEmail()
                //throw new Error()
                await email.send(user.email, token)
                await t.commit()
            }catch (e){
                await t.rollback()
                return res.status(500).json({
                    status:'error',
                    message:'Server error. Try again later'
                })
            }


        }


        return res.json({status: 'success'})


    }
}

module.exports=new PasswordRecoveryController()