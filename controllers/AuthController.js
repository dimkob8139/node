const {User} = require("../models/User");
const bcrypt = require('bcrypt');
const jwt=require('jsonwebtoken')

class AuthController{
    async singUp(req,res){
        console.log(req.body)
        const saltRounds = 10;
        const salt = bcrypt.genSaltSync(saltRounds);
        let hash = bcrypt.hashSync(req.body.password, salt);

        const user = await User.create({
            firstname:req.body.firstname,
            lastname:req.body.lastname,
            email: req.body.email,
            password:hash,
            role:req.body.role
        });
         delete (user.dataValues.password)
         return res.json(user)
    }


    async login(req,res){
     const   {email,password}=req.body
      let user= await User.scope('withPassword').findOne({
            where:{
                email: email
            }
        })
        if(!user){
            return res.status(422).json({
                errors:{
                    email:"wrong password or email"
                }
            })
        }


       let checkPassword= bcrypt.compareSync(password,user.password); // true

        if(!checkPassword){
            return res.status(422).json({
                errors:{
                    email:"wrong password or email"
                }
            })
        }

        const token=jwt.sign({id:user.id},
            process.env.SECRET_KEY,
            {
            expiresIn: "7d"
        });
        
        return res.json({token:token})

     }
}

module.exports=new AuthController()