'use strict';

const UserRoles = require("../services/UserRoles");
const {faker}=require('@faker-js/faker')
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    let users=[];

   function createRandomUser() {
     return {
       userId: faker.datatype.uuid(),
       name: faker.internet.userName(),
       email: faker.internet.email(),
       password: faker.internet.password(),
       avatar: faker.image.avatar(),
      role:UserRoles.FREELANCER,
       createdAt:new Date(),
       updatedAt:new Date()

     };
   }
    Array.from({ length: 10 }).forEach(() => {
      users.push(createRandomUser());
    });
    return queryInterface.bulkInsert('users',users)

  },
  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
