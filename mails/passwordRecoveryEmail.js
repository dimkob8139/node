const {Mail}=require('./mail')

class PasswordRecoveryEmail extends Mail{
    async send(to,token) {
        const url=process.env.FRONT_URL+'/password/reset?email='+to+'&token='+ token

        await this.transporter.sendMail({
            from: this.from, // sender address
            to: to, // list of receivers
            subject: "Email for password forgot", // Subject line
            html: 'For password recovery go to <a href="'+ url +'">link</a>', // html body
        });
    }
}

module.exports={
    PasswordRecoveryEmail:PasswordRecoveryEmail
}