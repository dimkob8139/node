const { Sequelize } = require('sequelize');
require('dotenv').config()


// const sequelize = new Sequelize(process.env.DB_NAME,process.env.DB_USERNAME, process.env.DB_PASSWORD, {
//     host: process.env.DB_HOST,
//     port:process.env.DB_PORT,
//     dialect:'postgres'
// });

const sequelize = new Sequelize('postgres', 'postgres', '1234', {
    host: 'localhost',
    port: '5433',
    dialect:  'postgres'
});
module.exports=sequelize
