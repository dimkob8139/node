const error = require("express/lib/request");


const {validationResult}= require("express-validator");



class BaseValidator{
    constructor() {
        this.reporter=(req,res,next)=>
        {
            this.createValidators()
            const errors = validationResult(req);
            //console.log(errors)
            if (!errors.isEmpty()) {
                let validationErrors={}
                for(const errors of errors.array()){
                    validationErrors[error.param]=error.msg

                }
                return res.status(422).json({ errors: validationErrors });
            }
            next()
        }
        this.add=()=>{
            return[
                this.createValidators(),
                this.reporter()
            ]
        }
    }


    createValidators=()=>[]

}

module.exports={
    validator:BaseValidator
}