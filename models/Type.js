const sequelize=require('../db')
const {Model,DataTypes}=require('sequelize')

class Type extends Model {}

Type.init({
    // Model attributes are defined here
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    }

}, {
    // Other model options go here
    tableName:'types',
    sequelize, // We need to pass the connection instance
    modelName: 'Type' // We need to choose the model name
});

// the defined model is the class itself
console.log(Type === sequelize.models.Type); // true

module.exports={Type:Type}