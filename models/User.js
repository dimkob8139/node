const sequelize=require('../db')
const UserRoles = require("../services/UserRoles");
const {Model,DataTypes}=require('sequelize')
const {Country} = require("./Country");
const {Level} = require("./Level");

class User extends Model {}

User.init({
    // Model attributes are defined here
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    firstname: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
        // allowNull defaults to true
    },
    password:{
        type: DataTypes.STRING,
        allowNull: false,
        select:false
    },
    role:{
        type: DataTypes.ENUM(UserRoles.ADMIN, UserRoles.FREELANCER, UserRoles.EMPLOYER),
        allowNull: false
    },
    description:{
        type: DataTypes.TEXT,
        allowNull: true
    },
    rate:{
        type: DataTypes.DOUBLE,
        allowNull: true
    },
    position:{
        type: DataTypes.DOUBLE,
        allowNull: true
    },
    avatar:{
        type: DataTypes.STRING,
        allowNull: true
    },
    country_id:{
        type: DataTypes.INTEGER,

        references: {
            // This is a reference to another model
            model: Country,

            // This is the column name of the referenced model
            key: 'id'
        }
    },
    level_id:{
        type: DataTypes.INTEGER,

        references: {
            // This is a reference to another model
            model: Level,

            // This is the column name of the referenced model
            key: 'id'
        }
    }
}, {
    // Other model options go here
    defaultScope:{
        attributes:{exclude:['password']},
    },
    scopes:{
        withPassword:{
            attributes:{ },
        }

    },
    tableName:'users',
    sequelize, // We need to pass the connection instance
    modelName: 'User' // We need to choose the model name
});

// the defined model is the class itself
console.log(User === sequelize.models.User); // true

module.exports={User:User}