const sequelize=require('../db')
const {Model,DataTypes}=require('sequelize')
const {Country} = require("./Country");
const {Level} = require("./Level");

class PasswordReset extends Model {}

PasswordReset.init({
    // Model attributes are defined here

    email: {
        type: DataTypes.STRING,
        allowNull: false
        // allowNull defaults to true
    },
    token:{
        type: DataTypes.STRING,
        allowNull: false
    },

}, {
    // Other model options go here
indexes:[
    {
        unique:true,
        fields:['email','token']
    }
],

    tableName:'password_resets',
    sequelize, // We need to pass the connection instance
    modelName: 'PasswordReset' // We need to choose the model name
});

// the defined model is the class itself


module.exports={PasswordReset:PasswordReset}