const sequelize=require('../db')
const {Model,DataTypes}=require('sequelize')

class Language extends Model {}

Language.init({
    // Model attributes are defined here
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    }

}, {
    // Other model options go here
    tableName:'languages',
    sequelize, // We need to pass the connection instance
    modelName: 'Language' // We need to choose the model name
});

// the defined model is the class itself
console.log(Language === sequelize.models.Language); // true

module.exports={Language:Language}