const crypto=require('node:crypto')

class GenerateUniqToken{
    generateToken(){
        return crypto.randomBytes(30).toString('hex')
    }
}
module.exports=new GenerateUniqToken()