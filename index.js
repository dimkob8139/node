const express = require('express')
const app = express()

const sequelize=require('./db')
require('dotenv').config()
const {UserModel, User}=require('./models/User')
const router=require('./routes/index')
const bodyParser=require('body-parser')
const upload=multer();
const { engine } =require('express-handlebars');
const port = process.env.PORT || 3000

app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(upload.array());
app.engine('handlebars', engine());
app.set('view engine', 'handlebars');
app.set('views', './views');

app.use(express.static('public'))

app.use('/',router)
app.get('/', (req, res) => {
  res.send('Hello World!')
})

const start=async ()=> {
  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
    await sequelize.sync({ alter: true });

  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }

  app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
  })
}
start()