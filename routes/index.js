const Router=require('express')
const router=new Router()
const mainRouter=require('./mainRouter')
const apiRateLimiterMiddleware=require('../middlewares/apiRateLimiterMiddleware')

router.use('/',apiRateLimiterMiddleware,mainRouter)

module.exports=router
