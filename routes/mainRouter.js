const Router=require('express')
const router=new Router()

const authMiddleware=require('../middlewares/authMiddleware')

const AuthController=require('../controllers/AuthController')
const UserController=require('../controllers/UserController')
const StaticPageController=require('../controllers/StaticPageController')
const PasswordRecoveryController = require('../controllers/PasswordRecoveryController');
const SingUpValidator=require('../validators/SingUpValidator')


router.post('/singup',[SingUpValidator.validate.add()],AuthController.singUp)
router.post('/login',AuthController.login)
router.post('/password/forgot',PasswordRecoveryController.passwordForgot)

router.get('/users',[authMiddleware],UserController.findAll)
router.get('/documetation',StaticPageController.documentation)

//router.post('/profile',UserController.profile)


module.exports=router

